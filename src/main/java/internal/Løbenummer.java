package internal;

import java.io.Serializable;

/**
 * Javad A. S. (s224792)
 */
public class Løbenummer implements Serializable {
    private int nuværendeLøbenummer;

    public Løbenummer() {
        this.nuværendeLøbenummer = 0;
    }

    int getLøbenummer() {
        nuværendeLøbenummer++;
        return nuværendeLøbenummer;
    }
}
