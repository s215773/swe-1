package internal;

/**
 * Magnus M. (s215773)
 */
public class SystemAppException extends Exception {

    public SystemAppException(String message) {
        super(message);
    }

}
