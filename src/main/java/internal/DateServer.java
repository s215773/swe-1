package internal;

/**
 * Javad A. S. (s224792)
 */
public interface DateServer {
    UgeDato getUgeDato();
}
