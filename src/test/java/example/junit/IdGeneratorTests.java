package example.junit;

import internal.IDGenerator;
import internal.Løbenummer;


/**
 * Hjalte N. (s224772)
 */
public class IdGeneratorTests {
	IDGenerator idGenerator;

	@org.junit.Before()
	public void testConstructor() {
		idGenerator = new IDGenerator();
	}

	@org.junit.Test()
	public void testGettersAndSetters() {
		idGenerator.getLøbenummer();
		idGenerator.setLøbenummer(new Løbenummer());
	}
}
